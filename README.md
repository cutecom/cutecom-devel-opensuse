# opensuse-qt5-devel

A Opensuse Tumbleweed based image with packages for Qt5 development included.


# Instructions

```
$git clone git@gitlab.com:cutecom/cutecom-devel-opensuse.git
$cd cutecom-devel-opensuse
# make changes

# make sure to use the most recent base image
docker pull opensuse/tumbleweed

#inspired by https://container-solutions.com/tagging-docker-images-the-right-way/

$git log -1 

$docker build -t
cutecom/cutecom-devel-opensuse:<GIT-HEAD-SHA1-FROM-PREVIOUS-COMMNAD> -t cutecom/cutecom-devel-opensuse:latest .

$docker login
Login with your Docker ID to push and pull

$docker images

$docker push cutecom/cutecom-devel-opensuse
```

Head over to https://hub.docker.com/r/cutecom/cutecom-devel-opensuse/ to verify result
